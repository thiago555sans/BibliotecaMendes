package conexao;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class Conexao {
	public static Connection getConexao() {
		
		Connection conexao = null;
		
		String servido = "localhost";
		String porta =  "3306";
		String usuario = "root";
		String senha = "";
		String dataBase = "biblioteca3";
		String jdbc = "jdbc:mysql://";
		String driveName = "com.mysql.jdbc.Driver";
		String url = jdbc + servido + ":" + porta + "/" + dataBase;
		
			try {
				Class.forName(driveName);
				conexao = DriverManager.getConnection(url+"?useSSL=false", usuario, senha);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		
		return conexao;
	}
	
	
	
	


}
