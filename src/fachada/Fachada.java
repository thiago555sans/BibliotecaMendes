package fachada;

import java.util.ArrayList;
import java.util.List;

import dao.AutorDao;
import dao.ClassificacaoDao;
import dao.MidiaDao;
import dao.ObraDao;
import exception.AutorNaoEncontradoException;
import exception.ClassificacaoJaExisteException;
import exception.ExisteObraCadastraComEsseObject;
import exception.JaCadastradoException;
import exception.MidiaNaoEncontradoException;
import exception.ClassificacaoJaExisteException;
import exception.NaoEncontradoException;
import exception.VazioException;
import modelo.Autor;
import modelo.Classificacao;
import modelo.Midia;
import modelo.Obra;

public class Fachada {

    private int sizeListaClassificacao;
    private ClassificacaoDao classificacaoDao = new ClassificacaoDao();
    private ObraDao obraDao = new ObraDao();
    private AutorDao autorDao = new AutorDao();
    private MidiaDao midiaDao = new MidiaDao();

    //Metodo para cadastra uma Obras
    public void cadastrarObra(Obra obra, List<Midia> midias, List<Autor> autores, Integer numero) throws JaCadastradoException, VazioException {
        if ((!(obra.getNome() == null)) || (!(obra.getLingua() == null)) || (!obra.getNome().trim().equals("")) || (!obra.getLingua().trim().equals(""))) {

            for (Autor autor : autores) {

                if (autor.getNome() == null || autor.getNome().trim().equals("") || autor.getNacionalidade() == null || autor.getNacionalidade().trim().equals("")) {

                    throw new VazioException();
                }
            }
            for (Midia midia : midias) {
                if (midia.getNome() == null || midia.getNome().trim().equals("")) {
                    throw new VazioException();
                }
            }
            if (!obraDao.verificaObra(obra.getNome(), obra.getLingua())) {
                List<Classificacao> lista = classificacaoDao.pegaClassificacoes();
                obra.setClassificao(lista.get(numero));
                obraDao.cadastraLivro(obra, midias, autores);
            } else {
                throw new JaCadastradoException();
            }
        } else {
            throw new VazioException();
        }

    }

    //Retorna uma Lista de Obras
    public List<Obra> listarObras() throws NaoEncontradoException {
        List<Obra> array = obraDao.pegaObras();
        return array;
    }

    //Retorna um Lista de Autores
    public List<Autor> listarAutores() throws NaoEncontradoException {
        List<Autor> array = autorDao.pegaAutores();
        if (array.size() == 0) {
            throw new NaoEncontradoException();
        }
        String lista = "";
        for (Autor autor : array) {
            lista = lista + autor + "\n";
        }
        return array;
    }

    //Busca todas as Obras por um Nome
    public List<Obra> buscaObraNome(String nome) throws NaoEncontradoException {
        List<Obra> obras = obraDao.buscaPorNome(nome);
//        String lista = "";
//        if (obras.size() == 0) {
//            throw new NaoEncontradoException();
//        }
//        for (Obra obra : obras) {
//            lista = lista + obra + "\n";
//        }
        return obras;

    }

    //Busca  todas as Obras que tem um tipo especifico de Autor
    public List<Obra> buscaObraAutor(Autor autor) throws NaoEncontradoException, AutorNaoEncontradoException {
        if (autorDao.trataAutor(autor) != null) {
            Integer id = autorDao.trataAutor(autor);
            List<Obra> obras = obraDao.buscaPorAutor(id);
            if (obras.size() != 0) {
                String lista = "";
                for (Obra obra : obras) {
                    lista = lista + obra + "\n";
                }
                return obras;
            } else {
                throw new NaoEncontradoException();
            }

        } else {
            throw new AutorNaoEncontradoException();
        }
    }

    //Cadastra Uma Classificacao
    public void cadastarClassificacao(Classificacao classificacao) throws ClassificacaoJaExisteException, VazioException {
        if (classificacao.getNome() == null || classificacao.getNome().trim().equals("")) {
            throw new VazioException();
        }

        if (!classificacaoDao.existeClassificacao(classificacao.getNome())) {
            classificacaoDao.cadastrarClassificacao(classificacao);

        } else {
            throw new ClassificacaoJaExisteException();
        }

    }

    //Retorna a quantidade de Classifica��o no banco
    public int getSizeListaClassificacao() {
        return sizeListaClassificacao;
    }

    //Retorna uma Lista de Classisfica��o
    public String listarClassificacao() {
        List<Classificacao> array = classificacaoDao.pegaClassificacoes();

        String lista = "";
        for (Classificacao classificacao : array) {
            lista = lista + classificacao + "\n";
        }
        return lista;
    }

    //Retorna uma Lista de Classifica��o para o Cadastro de uma Obra
    public String listaCadClassificacao() {
        List<Classificacao> array = classificacaoDao.pegaClassificacoes();
        this.sizeListaClassificacao = array.size();
        String nomeclas = "";
        for (Classificacao classificacao2 : array) {
            nomeclas = nomeclas + "- " + array.lastIndexOf(classificacao2) + " " + classificacao2.getNome() + "\n";
        }
        return nomeclas;
    }

    //Retorna uma Lista de Midias
    public String listarMidias() throws NaoEncontradoException {
        List<Midia> array = midiaDao.pegaMidias();
        String lista = "";
        if (array.size() == 0) {
            throw new NaoEncontradoException();
        }
        for (Midia midia : array) {
            lista = lista + midia.getNome() + "\n";
        }
        return lista;
    }

    //Remove  uma Obra
    public void removeObra(String nome, String lingua) throws NaoEncontradoException {

        if (obraDao.getIdNome(nome, lingua) != null) {
            Integer id = obraDao.getIdNome(nome, lingua);
            System.out.println(id);
            obraDao.removeObra(id);
        } else {
            throw new NaoEncontradoException();
        }
    }

    //Remove uma Classifica�ao
    public void removeClassificacao(String nome) throws NaoEncontradoException, ExisteObraCadastraComEsseObject {
        if (classificacaoDao.existeClassificacao(nome)) {
            Integer id = classificacaoDao.getIdNome(nome);
            if (!classificacaoDao.verificaSeTemObra(id)) {
                classificacaoDao.removerClassificacao(id);
            } else {
                throw new ExisteObraCadastraComEsseObject();
            }
        } else {
            throw new NaoEncontradoException();
        }
    }

    //Busca  todas as Obras que tem um tipo especifico de Midia
    public List<Obra> buscaObraMidia(Midia midia) throws NaoEncontradoException, MidiaNaoEncontradoException {
        if (midiaDao.trataMidia(midia) != null) {
            Integer id = midiaDao.trataMidia(midia);
            List<Obra> obras = obraDao.buscaPorMidia(id);
            if (obras.size() != 0) {
                String lista = "";
                for (Obra obra : obras) {
                    lista = lista + obra + "\n";
                }
                return obras;
            } else {
                throw new NaoEncontradoException();
            }

        } else {
            throw new MidiaNaoEncontradoException();
        }
    }

    /////////////////////////////// Atualizar ////////////////////////////////////////
    //Retorna uma Obra para o Autualizar Obra
    public Obra pegaObra(String nome, String lingua) throws VazioException, NaoEncontradoException {
        if (!(nome == null) || !(lingua == null)) {
            if (obraDao.verificaObra(nome, lingua)) {
                Integer id = obraDao.getIdNome(nome, lingua);
                Obra obra = new Obra();
                obra = obraDao.getObra(id);
                return obra;
            } else {
                throw new NaoEncontradoException();
            }

        } else {
            throw new VazioException();
        }

    }

    //Retorna os Autores de Uma Obra
    public String listarAutoresObra(Obra obra) {
        String listaAutores = "";
        for (Autor autor3 : obra.getAutores()) {
            listaAutores = listaAutores + "-" + obra.getAutores().lastIndexOf(autor3) + " " + autor3.getNome() + "\n";
        }
        return listaAutores;
    }

    //Lista as Midias de uma Obra
    public String listaMidiaObra(Obra obra) {
        String lista = "";
        for (Midia midia : obra.getMidias()) {
            lista = lista + " - " + obra.getMidias().lastIndexOf(midia) + " " + midia.getNome() + "\n";
        }
        return lista;
    }

    public void updateRemoveAutor(Integer id, Autor autor) {
        obraDao.removerAutor(id, autor);
    }

    public void updateRemoveMidia(Integer id, Midia midia) {
        obraDao.removerMidia(id, midia);

    }

    public void update(Obra obra, List<Autor> autores, List<Midia> midias, Integer numero) throws VazioException, JaCadastradoException {
        //Verifiva se o nome atualizado � nulo
        if ((!(obra.getNome() == null)) || (!(obra.getLingua() == null))) {

            //Verifica se tem novos autores adicionado
            if (autores.size() != 0) {

                for (Autor autor : autores) {
                    System.out.println("entrou no 1�for");
                    //Verifica sem tem campos nulo nos autores adicionado
                    if (autor.getNome() == null || autor.getNacionalidade() == null) {
                        throw new VazioException();
                    }
                    for (Autor autor2 : obra.getAutores()) {
                        System.out.println("entrou no 2�for");
                        if (autor.getNome().equalsIgnoreCase(autor2.getNome()) && autor.getNacionalidade().equalsIgnoreCase(autor2.getNacionalidade())) {
                            System.out.println("entrou no facahada");
                            autores.remove(autor);
                            throw new JaCadastradoException();
                        }

                    }
                }
                obraDao.cadastraAutorNaObra(autores, obra);

            }
            //Verifica se tem novas midias adicionado
            if (midias.size() != 0) {

                for (Midia midia : midias) {
                    //Verifica sem tem campos nulos nas midias adicionado
                    if (midia.getNome() == null) {
                        throw new VazioException();
                    }
                    for (Midia midia2 : obra.getMidias()) {
                        if (midia.getNome().equalsIgnoreCase(midia2.getNome())) {
                            throw new JaCadastradoException();
                        }
                    }
                }
                obraDao.cadastraMidiaObra(midias, obra);
            }

            //verifica se a classifica��o foi alterada
            if (numero != null) {
                List<Classificacao> lista = classificacaoDao.pegaClassificacoes();
                obra.setClassificao(lista.get(numero));
            }

            obraDao.updateObra(obra);

        } else {
            throw new VazioException();
        }

    }

}
