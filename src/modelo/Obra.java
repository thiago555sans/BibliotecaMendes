package modelo;

import java.util.ArrayList;
import java.util.List;

public class Obra {

	private Integer id;
	private String nome;
	private String lingua;
	private List<Autor> autores = new ArrayList<>();
	private List<Midia> midias =  new ArrayList<>();
	private Classificacao classificao;
	
	public Obra() {}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getLingua() {
		return lingua;
	}

	public void setLingua(String lingua) {
		this.lingua = lingua;
	}

	public List<Autor> getAutores() {
		return autores;
	}

	public void setAutores(Autor autor) {
		this.autores.add(autor);
	}

	public List<Midia> getMidias() {
		return midias;
	}

	public void setMidias(Midia midia) {
		this.midias.add(midia);
	}

	public Classificacao getClassificao() {
		return classificao;
	}

	public void setClassificao(Classificacao classificao) {
		this.classificao = classificao;
	}

	@Override
	public String toString() {
		return "Obra [ nome=" + nome + ", lingua=" + lingua + ","
				+ "\n \t Autores= " + autores + ", "
				+ " \n \t Midias= " + midias + ", "
				+ "\n \t Classificao= " + classificao + " ]";
	}

    public String retornaNomeAutor(){
            List<Autor> autoresLista = this.autores;
            String lista="";
            for (Autor autor : autoresLista) {
                lista+=autor.getNome()+" ";
            }
            return lista;
        }
        
        public String retornNomeMidia(){
            String lista="";
            List<Midia> midias1 = this.getMidias();
            for (Midia midia : midias1) {
                lista+=midia.getNome()+" ";
            }
            return lista;
        }
	
	
	
	
}