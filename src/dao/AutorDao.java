package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.PreparedStatement;

import conexao.Conexao;
import modelo.Autor;

public class AutorDao {
	
	private List<Autor> autores = new ArrayList<>();
	
	//Cadastra um Autor no banco
	public Integer cadastraAutor(Autor autor) {
		Connection conexao = null;
		PreparedStatement query = null;
		ResultSet rs = null;
		Integer id = 0;
		try {
			String sql = "INSERT INTO autor(nome, nacionalidade) VALUE(?, ?)";
			conexao = Conexao.getConexao();
			query = (PreparedStatement) conexao.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			query.setString(1, autor.getNome());
			query.setString(2, autor.getNacionalidade());
			query.execute();
			rs = query.getGeneratedKeys();
			while(rs.next()) {
				id = rs.getInt(1);
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				conexao.close();
				query.close();
			}catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return id;
	}
	
	//Retorna uma Lista de todos os Autores do Banco
	public List<Autor> pegaAutores(){
		Connection conexao = null;
		PreparedStatement query = null;
		ResultSet rs = null;
		Autor autor = null;
		
		try {
			conexao = Conexao.getConexao();
			query = (PreparedStatement) conexao.prepareStatement("SELECT * FROM autor");
			rs = query.executeQuery();
			
			while(rs.next()) {
				autor = new Autor(rs.getInt("id"), rs.getString("nome"), rs.getString("nacionalidade"));
				autores.add(autor);
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				conexao.close();
				query.close();
				rs.close();
			}catch (SQLException e) {
				e.printStackTrace();
			}
		}		
		return autores;
	}
	
	
	//Retorna um Autor que tem o Id informado
	public Autor getAutor(Integer id){
		Connection conexao = null;
		PreparedStatement query = null;
		ResultSet rs = null;
		
		try {
			conexao = Conexao.getConexao();
			query = (PreparedStatement) conexao.prepareStatement("SELECT * FROM autor WHERE id = ?");
			query.setInt(1, id);
			rs = query.executeQuery();
			
			if(rs.next()) {
				Autor autor = new Autor();
				autor.setId(rs.getInt("id"));
				autor.setNome(rs.getString("nome"));
				autor.setNacionalidade(rs.getString("nacionalidade"));
				return autor;
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				conexao.close();
				query.close();
				rs.close();
			}catch (SQLException e) {
				e.printStackTrace();
			}
		}		
		return null;
	}
	
	
	// remover um Autor pelo id
	public void remover(Integer id) {
		Connection conexao = null;
		PreparedStatement query = null;
		
		try {
			String sql = "DELETE FROM autor WHERE id = ?"; 
			conexao = Conexao.getConexao();
			query = (PreparedStatement) conexao.prepareStatement(sql);
			query.setInt(1, id);
			query.execute();
		}catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				conexao.close();
				query.close();
			}catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	//Retorna um Lista de Id da Lista de Autores Informado
	//Se o Autor ja estiver cadastrado ele pega Id no banco
	//Se nao estiver cadastrado ele cadastra e pega o id
	public List<Integer> trataAutores(List<Autor> lista) {
		Connection conexao = null;
		PreparedStatement query = null;
		ResultSet rs = null;
		List<Integer> listaId = new ArrayList<>();
		try {
			String sql = "SELECT *  FROM autor WHERE nome = ? AND nacionalidade = ?";
			conexao = Conexao.getConexao();
			for (Autor autor : lista) {
				query = (PreparedStatement) conexao.prepareStatement(sql);
				query.setString(1, autor.getNome());
				query.setString(2, autor.getNacionalidade());
				
				rs = query.executeQuery();
				if(rs.next()) {
					listaId.add(rs.getInt("id"));
				}else {
					listaId.add(cadastraAutor(autor));
				}
			}
			
			return listaId;
			
		}catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				conexao.close();
				query.close();
				rs.close();
			}catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return null;
		
	}
	
	
	//Retorna o id de Autor;
	public Integer trataAutor(Autor autor) {
		Connection conexao = null;
		PreparedStatement query = null;
		ResultSet rs = null;
		Integer id = null;
		
		try {
			String sql = "SELECT * FROM autor WHERE nome = ? AND nacionalidade = ?";
			conexao = Conexao.getConexao();
			query = (PreparedStatement) conexao.prepareStatement(sql);
			query.setString(1, autor.getNome());
			query.setString(2, autor.getNacionalidade());
			rs = query.executeQuery();
			if(rs.next()) {
				id= rs.getInt("id");
			}
			
		}catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				conexao.close();
				query.close();
				rs.close();
			}catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return id;
	}


}
