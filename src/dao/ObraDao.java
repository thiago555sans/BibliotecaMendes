package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.transform.Result;

import com.mysql.jdbc.PreparedStatement;

import conexao.Conexao;
import modelo.Autor;
import modelo.Midia;
import modelo.Obra;

public class ObraDao {
	
	
	private AutorDao autorDao = new AutorDao();
	private MidiaDao midiaDao = new MidiaDao();
	private ClassificacaoDao classificacaoDao = new ClassificacaoDao();

	
	
	//Metodo para cadastrar uma Obra
	public void cadastraLivro(Obra obra, List<Midia> midias, List<Autor> autores) {
		Connection conexao = null;
		PreparedStatement query = null;
		ResultSet rs = null;
		List<Integer> listaIdAutores = new ArrayList<>();
		List<Integer> listaIdMidia = new ArrayList<>();
		Integer idObra = null;
	
		try {
			
			//Metodo que retorna uma lista de id de autores
			listaIdAutores = autorDao.trataAutores(autores);
			
			//Metodo que retorna uma lista de id de midias
			listaIdMidia = midiaDao.trataMidias(midias);
			
			String sql = "INSERT INTO obra(nome, lingua, id_classificacao)value(?, ?, ?)";
			conexao = Conexao.getConexao();
			query = (PreparedStatement) conexao.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			query.setString(1, obra.getNome());
			query.setString(2, obra.getLingua());
			query.setInt(3, obra.getClassificao().getId());
			query.execute();
			rs = query.getGeneratedKeys();
			
			while(rs.next()) {
				idObra = rs.getInt(1);
			}
			sql ="INSERT INTO obra_autor(id_obra, id_autor) VALUES(?,?)";
			for (Integer idAutor : listaIdAutores) {
				query = (PreparedStatement) conexao.prepareStatement(sql);
				query.setInt(1, idObra);
				query.setInt(2, idAutor);
				query.execute();
			}
			sql ="INSERT INTO obra_midia(id_obra, id_midia) VALUES(?,?)";
			for (Integer idMidia : listaIdMidia) {
				query = (PreparedStatement) conexao.prepareStatement(sql);
				query.setInt(1, idObra);
				query.setInt(2, idMidia);
				query.execute();
			}
			
		}catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				conexao.close();
				query.close();
				rs.close();
			}catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	
	
	//Retorna todas as obras do Banco
	public List<Obra> pegaObras(){
		List<Obra> obras = new ArrayList<>();
		Connection conexao = null;
		PreparedStatement query = null;
		ResultSet rs =  null;
		ResultSet rs2 =  null;
		
		try {
			String sql = "SELECT * FROM obra";
			conexao =  Conexao.getConexao();
			query = (PreparedStatement) conexao.prepareStatement(sql);
			rs = query.executeQuery();
			
			while (rs.next()) {
				Obra obra = new Obra();
				obra.setId(rs.getInt("id"));
				obra.setNome(rs.getString("nome"));
				obra.setLingua(rs.getString("lingua"));
				obra.setClassificao(classificacaoDao.getClassificacao(rs.getInt("id_classificacao")));
				
				sql = "SELECT id_autor FROM obra_autor WHERE id_obra = ?";
				query = (PreparedStatement) conexao.prepareStatement(sql);
				query.setInt(1, obra.getId());
				rs2 = query.executeQuery();
				
				while(rs2.next()) {
					obra.setAutores(autorDao.getAutor(rs2.getInt("id_autor")));
				}
				
				sql = "SELECT id_midia FROM obra_midia WHERE id_obra = ?";
				query = (PreparedStatement) conexao.prepareStatement(sql);
				query.setInt(1, obra.getId());
				rs2 = query.executeQuery();
				while(rs2.next()) {
					obra.setMidias(midiaDao.getMidia(rs2.getInt("id_midia")));
				}
				
				obras.add(obra);
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				conexao.close();
				query.close();
				rs.close();
				rs2.close();
			}catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return obras;
	}
	
	//Retorna uma obra pelo Id informado
	public Obra getObra(Integer id) {
		Connection conexao = null;
		PreparedStatement query =  null;
		ResultSet rs = null;
		ResultSet rs2 = null;
		
		try {
			String sql = "SELECT * FROM obra WHERE id = ?";
			conexao = Conexao.getConexao();
			query = (PreparedStatement) conexao.prepareStatement(sql);
			query.setInt(1, id);
			rs = query.executeQuery();
			if(rs.next()) {
				Obra obra = new Obra();
				obra.setId(rs.getInt("id"));
				obra.setNome(rs.getString("nome"));
				obra.setLingua(rs.getString("lingua"));
				obra.setClassificao(classificacaoDao.getClassificacao(rs.getInt("id_classificacao")));
					
				sql = "SELECT id_autor FROM obra_autor WHERE id_obra = ?";
				query = (PreparedStatement) conexao.prepareStatement(sql);
				query.setInt(1, obra.getId());
				rs2 = query.executeQuery();
					
				while(rs2.next()) {
					obra.setAutores(autorDao.getAutor(rs2.getInt("id_autor")));
				}
					
				sql = "SELECT id_midia FROM obra_midia WHERE id_obra = ?";
				query = (PreparedStatement) conexao.prepareStatement(sql);
				query.setInt(1, obra.getId());
				rs2 = query.executeQuery();
				while(rs2.next()) {
					obra.setMidias(midiaDao.getMidia(rs2.getInt("id_midia")));
				}
					
				return obra;
			}
			
		}catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				conexao.close();
				query.close();
				rs.close();
				rs2.close();
			}catch (SQLException e) {
				e.printStackTrace();
			}
		}		
		return null;
	}
	
	//Retorna uma lista de obras que tem o nome informado
	public List<Obra> buscaPorNome(String nome){
		Connection conexao =  null;
		PreparedStatement query  = null;
		ResultSet rs = null;
		List<Obra> obras =  new ArrayList<>();
		
		try {
			String sql = "SELECT * FROM obra WHERE nome LIKE ?";
			conexao = Conexao.getConexao();
			query = (PreparedStatement) conexao.prepareStatement(sql);
			query.setString(1, nome + "%");
			rs = query.executeQuery();
			
			while (rs.next()) {
				Obra obra = new Obra();
				obra = getObra(rs.getInt("id"));
				obras.add(obra);			
			}
			
			}catch (Exception e) {
				e.printStackTrace();
			}finally {
				try {
					conexao.close();
					query.close();
					rs.close();
				}catch (Exception e) {
					e.printStackTrace();
				}
				
			}
		return obras;
		}

	
	
	//Remover uma obra pelo id
	public void removeObra(Integer id) {
		
		Connection conexao = null;
		PreparedStatement query = null;
		
		try {
			String sql = "DELETE FROM obra_autor WHERE id_obra= ?";
			conexao = Conexao.getConexao();
			query = (PreparedStatement) conexao.prepareStatement(sql);
			query.setInt(1, id);
			
			query.execute();
			
			sql = "DELETE FROM obra_midia WHERE id_obra= ?";
			query = (PreparedStatement) conexao.prepareStatement(sql);
			query.setInt(1, id);
			
			query.execute();
			
			sql = "DELETE FROM obra WHERE id = ?";
			query = (PreparedStatement) conexao.prepareStatement(sql);
			query.setInt(1, id);
			
			
			query.execute();
		}catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				conexao.close();
				query.close();
			}catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}


	//Retorna uma Lista de Obras que tem o Autor informado
	public List<Obra> buscaPorAutor(Integer id) {
		List<Obra> obras = new ArrayList<>();
		Connection conexao = null;
		PreparedStatement query = null;
		ResultSet rs = null;
		try {
			String sql = "SELECT id_obra FROM obra_autor WHERE id_autor  = ?";
			conexao = Conexao.getConexao();
			query = (PreparedStatement) conexao.prepareStatement(sql);
			query.setInt(1, id);
			rs = query.executeQuery();
			while(rs.next()) {
				Obra obra = new Obra();
				obra = getObra(rs.getInt("id_obra"));
				obras.add(obra);
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				conexao.close();
				query.close();
				rs.close();
			}catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		
		return obras;
	}
	
	//Verifica se tem uma obra com o Nome e a Lingua Informado
	public boolean verificaObra(String nome, String lingua) {
		Connection conexao = null;
		PreparedStatement query = null;
		ResultSet rs = null;
		
		try {
			String sql = "SELECT * FROM obra WHERE nome = ? AND lingua = ?";
			conexao = Conexao.getConexao();
			query = (PreparedStatement) conexao.prepareStatement(sql);
			query.setString(1, nome);
			query.setString(2, lingua);
			rs = query.executeQuery();
			if(rs.next()) {
				return true;
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				conexao.close();
				query.close();
				rs.close();
			}catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return false;
	}
	
	
	
	
	
	//Retorna o Id de uma obra que tem o nome e a lingua informado
	public Integer getIdNome(String nome, String lingua) {
		Connection conexao = null;
		PreparedStatement query = null;
		ResultSet rs = null;
		Integer id = null;
		try {
			String sql = "SELECT *  FROM  obra WHERE nome = ? AND lingua = ?";
			conexao =Conexao.getConexao();
			query = (PreparedStatement) conexao.prepareStatement(sql);
			query.setString(1, nome);
			query.setString(2, lingua);
			rs = query.executeQuery();
			if(rs.next()) {
				id = rs.getInt("id");
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				conexao.close();
				query.close();
				rs.close();
			}catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return id;
	}
	
	//Retorna uma Lista de Obras que tem a midia informada
	public List<Obra> buscaPorMidia(Integer id) {
		List<Obra> obras = new ArrayList<>();
		Connection conexao = null;
		PreparedStatement query = null;
		ResultSet rs = null;
		try {
			String sql = "SELECT id_obra FROM obra_midia WHERE id_midia  = ?";
			conexao = Conexao.getConexao();
			query = (PreparedStatement) conexao.prepareStatement(sql);
			query.setInt(1, id);
			rs = query.executeQuery();
			while(rs.next()) {
				Obra obra = new Obra();
				obra = getObra(rs.getInt("id_obra"));
				obras.add(obra);
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				conexao.close();
				query.close();
				rs.close();
			}catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		
		return obras;
	}
	
	//Remover o Autor da table Obra_autor nao da tabela Autor
	public void removerAutor(Integer id, Autor autor) {
		Connection conexao =  null;
		PreparedStatement query = null;
		
		try {
			String sql = "DELETE FROM obra_autor WHERE id_autor = ? AND id_obra = ?";
			conexao = Conexao.getConexao();
			query = (PreparedStatement) conexao.prepareStatement(sql);
			query.setInt(1, autor.getId());
			query.setInt(2, id);
			query.execute();
		}catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				conexao.close();
				query.close();
			}catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	//Remover um Midia de uma obra
	public void removerMidia(Integer id, Midia midia) {
		Connection conexao = null;
		PreparedStatement query =  null;
		
		try {
			String sql = "DELETE FROM obra_midia WHERE id_midia = ? AND id_obra = ?";
			conexao = Conexao.getConexao();
			query = (PreparedStatement) conexao.prepareStatement(sql);
			query.setInt(1, midia.getId());
			query.setInt(2, id);
			query.execute();
		}catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				conexao.close();
				query.close();
			}catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	//Cadastra Um Novo autor em uma Obra;
	public void cadastraAutorNaObra(List<Autor> autores, Obra obra) {
		Connection conexao = null;
		PreparedStatement query = null;
		List<Integer> listaId =  new ArrayList<>();
		
		try {
			listaId = autorDao.trataAutores(autores);
			String sql = "INSERT INTO obra_autor(id_obra, id_autor) VALUES(?,?)";
			conexao = Conexao.getConexao();
			for (Integer integer : listaId) {
				query = (PreparedStatement) conexao.prepareStatement(sql);
				query.setInt(1, obra.getId());
				query.setInt(2, integer);
				query.execute();
			}
			
		}catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				conexao.close();
				query.close();
			}catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}



	public void cadastraMidiaObra(List<Midia> midias, Obra obra) {
		Connection conexao = null;
		PreparedStatement query = null;
		List<Integer> listaId = new ArrayList<>();
		try {
			listaId = midiaDao.trataMidias(midias);
			String sql = "INSERT INTO obra_midia(id_obra, id_midia) VALUES(?,?)";
			conexao = Conexao.getConexao();
			
			for (Integer integer : listaId) {
				query =  (PreparedStatement) conexao.prepareStatement(sql);
				query.setInt(1, obra.getId());
				query.setInt(2, integer);
				query.execute();
				
			}
			
		}catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				conexao.close();
				query.close();
			}catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}
	//METODO PARA ATUALIZAR
	public void updateObra(Obra obra) {
		Connection conexao =  null;
		PreparedStatement query = null;
		try {
			String sql ="UPDATE obra SET nome = ?, lingua = ?, id_classificacao = ? WHERE id = ?";
			conexao = Conexao.getConexao();
			query = (PreparedStatement) conexao.prepareStatement(sql);
			query.setString(1, obra.getNome());
			query.setString(2, obra.getLingua());
			query.setInt(3, obra.getClassificao().getId());
			query.setInt(4, obra.getId());
			query.executeUpdate();
		}catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try{
				conexao.close();
				query.close();
				
			}catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	
}
