package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;

import com.mysql.jdbc.PreparedStatement;

import conexao.Conexao;
import modelo.Classificacao;

public class ClassificacaoDao {
	
	//Cadastra um Classificacao
	public void cadastrarClassificacao(Classificacao classificacao) {
		Connection conexao = null;
		PreparedStatement  query = null;
		
		try {
			conexao = Conexao.getConexao();
			query = (PreparedStatement) conexao.prepareStatement("INSERT INTO classificacao(nome) VALUE(?)");
			query.setString(1, classificacao.getNome());
			query.execute();
			
		}catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				conexao.close();
				query.close();
			}catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	//Retorna a Classificacao do Id Informado
	public Classificacao getClassificacao(Integer id) {
		
		Connection conexao = null;
		PreparedStatement query = null;
		ResultSet rs = null;
		
		try {
			conexao = Conexao.getConexao();
			query = (PreparedStatement) conexao.prepareStatement("SELECT * FROM classificacao WHERE id = ?");
			query.setInt(1, id);
			rs = query.executeQuery();
			if(rs.next()) {
				Classificacao classificacao = new Classificacao();
				classificacao.setId(rs.getInt("id"));
				classificacao.setNome(rs.getString("nome"));
				return classificacao;
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				conexao.close();
				query.close();
				rs.close();
			}catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return null;
	}
	
	
	// Retorna todas as Classificacaes do Banco
	public List<Classificacao> pegaClassificacoes(){
		List<Classificacao> lista = new ArrayList<>();
		Connection conexao = null;
		PreparedStatement query = null;
		ResultSet rs = null;
		
		try {
			
			conexao = Conexao.getConexao();
			query = (PreparedStatement) conexao.prepareStatement("SELECT * FROM classificacao");
			rs = query.executeQuery();
			
			while(rs.next()) {
				Classificacao classificacao = new Classificacao();
				classificacao.setId(rs.getInt("id"));
				classificacao.setNome(rs.getString("nome"));
				lista.add(classificacao);
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				conexao.close();
				query.close();
				rs.close();
			}catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return lista;
	}

	//Verifica se tem uma Classificao com o Nome Informado
	public boolean existeClassificacao(String nome) {
		Connection conexao = null;
		PreparedStatement query = null;
		ResultSet rs = null;
		
		try {
			conexao = Conexao.getConexao();
			query = (PreparedStatement) conexao.prepareStatement("SELECT * FROM classificacao WHERE nome = ?");
			query.setString(1, nome);
			rs = query.executeQuery();
			if(rs.next()) {
				return true;
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				conexao.close();
				query.close();
				rs.close();
			}catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return false;
	}
	
	//Remove um Classificao do Banco
	public void  removerClassificacao(Integer id) {
		Connection conexao = null;
		PreparedStatement query =  null;
		
		try {
			String sql = "DELETE FROM classificacao WHERE id = ?";
			conexao = Conexao.getConexao();
			query = (PreparedStatement) conexao.prepareStatement(sql);
			query.setInt(1, id);
			query.execute();
		}catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				conexao.close();
				query.close();
				
			}catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	//Retorna O id da classificao que tem o nome informado
	public Integer getIdNome(String nome) {
		Connection conexao = null;
		PreparedStatement query =  null;
		ResultSet rs = null;
		Integer id =  null;
		
		try {
			String sql = "SELECT * FROM classificacao WHERE nome = ?";
			conexao = Conexao.getConexao();
			query = (PreparedStatement) conexao.prepareStatement(sql);
			query.setString(1, nome);
			rs = query.executeQuery();
			if(rs.next()) {
				id = rs.getInt("id");
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				conexao.close();
				query.close();
				rs.close();
			}catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return id;
	}
	
	//Verifica se tem a Classificao em alguma obra 
	public Boolean verificaSeTemObra(Integer id) {
		Connection conexao = null;
		PreparedStatement query = null;
		ResultSet rs = null;
		
		try {
			String sql = "SELECT * FROM obra WHERE id_classificacao = ?";
			conexao = Conexao.getConexao();
			query = (PreparedStatement) conexao.prepareStatement(sql);
			query.setInt(1, id);
			rs = query.executeQuery();
			if(rs.next()) {
				return true;
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				conexao.close();
				rs.close();
				query.close();
			}catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return false;
		
	}
	
	
	
	
}
