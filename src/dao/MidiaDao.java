package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.PreparedStatement;

import conexao.Conexao;
import modelo.Midia;

public class MidiaDao {
	
	
	//Cadastra uma Midia no Banco
	public Integer cadastrarMidia(Midia midia) {
		Connection conexao = null;
		PreparedStatement query = null;
		ResultSet rs =  null;
		Integer id = 0;
		
		try {
			conexao = Conexao.getConexao();
			query = (PreparedStatement) conexao.prepareStatement("INSERT INTO midia(nome) VALUE(?)", Statement.RETURN_GENERATED_KEYS);
			query.setString(1, midia.getNome());
			query.execute();
			rs = query.getGeneratedKeys();
			while(rs.next()) {
				id = rs.getInt(1);
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				conexao.close();
				query.close();
			}catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return id;
	}
	
	
	//Retorna a Midia do id Informado
	public Midia getMidia(Integer id) {
		
		Connection conexao = null;
		PreparedStatement query = null;
		ResultSet rs = null;
		
		try {
			conexao = Conexao.getConexao();
			query = (PreparedStatement) conexao.prepareStatement("SELECT * FROM midia WHERE id = ?");
			query.setInt(1, id);
			rs = query.executeQuery();
			if(rs.next()) {
				
				Midia midia = new Midia();
				midia.setId(rs.getInt("id"));
				midia.setNome(rs.getString("nome"));
				return midia;
				
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				conexao.close();
				query.close();
				rs.close();
			}catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	
	//Retorna todas as Midia do banco
	public List<Midia> pegaMidias(){
		List<Midia> lista = new ArrayList<>();
		Connection conexao = null;
		PreparedStatement query =  null;
		ResultSet rs = null;
		
		try {
			
			conexao = Conexao.getConexao();
			query = (PreparedStatement) conexao.prepareStatement("SELECT * FROM midia");
			rs = query.executeQuery();
			
			while(rs.next()) {
				Midia midia = new Midia();
				midia.setId(rs.getInt("id"));
				midia.setNome(rs.getString("nome"));
				lista.add(midia);
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				conexao.close();
				query.close();
				rs.close();
			}catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return lista;
	}
	
		//Retorna um Lista de Id da Lista de Midias Informado
		//Se a Midia ja estiver cadastrada ele pega Id no banco
		//Se nao estiver cadastrado ele cadastra e pega o id
	public List<Integer> trataMidias(List<Midia> lista){
		List<Integer> listaId = new ArrayList<>();
		Connection conexao = null;
		PreparedStatement query = null;
		ResultSet rs = null;
		
		try {
			String sql = "SELECT * FROM midia WHERE nome = ?";
			conexao = Conexao.getConexao();
			for (Midia midia : lista) {
				query = (PreparedStatement) conexao.prepareStatement(sql);
				query.setString(1, midia.getNome());
				rs = query.executeQuery();
				
				if(rs.next()) {
					listaId.add(rs.getInt("id"));
				}else {
					listaId.add(cadastrarMidia(midia));
				}
			}
			return listaId;
		}catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				conexao.close();
				query.close();
				rs.close();
			}catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	//Retorna o Id da Midia Informada
	public Integer trataMidia(Midia midia) {
		Connection conexao = null;
		PreparedStatement query =  null;
		ResultSet rs =  null;
		Integer id = null;
		
		try {
			String sql = "SELECT * FROM midia WHERE nome = ?";
			conexao = Conexao.getConexao();
			query = (PreparedStatement) conexao.prepareStatement(sql);
			query.setString(1, midia.getNome());
			rs = query.executeQuery();
			if(rs.next()) {
				id = rs.getInt("id");
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				conexao.close();
				query.close();
				rs.close();
			}catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return id;
	}
}
