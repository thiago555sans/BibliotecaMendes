/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import exception.NaoEncontradoException;
import fachada.Fachada;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;
import modelo.Classificacao;
import modelo.Obra;

/**
 *
 * @author thiag
 */
public class TelaListaObras extends javax.swing.JFrame {

    Fachada fachada = new Fachada();
    Obra obra = new Obra();
    Classificacao classificacao = new Classificacao();

    /**
     * Creates new form TelaListaObras
     */
    public TelaListaObras() throws NaoEncontradoException {
        initComponents();
        carregarTabelaLivros();
    }

    private void carregarTabelaLivros() throws NaoEncontradoException {
        
        ArrayList<Obra> obras = (ArrayList<Obra>) fachada.listarObras();
        DefaultTableModel dtmLivros = (DefaultTableModel) jtListarLivros.getModel();
        for (Obra obra : obras) {
            Object dados[] = {obra.getNome(), obra.getLingua(), obra.retornNomeMidia(), obra.getClassificao().toString(), obra.retornaNomeAutor()};
            dtmLivros.addRow(dados);
        }

    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane2 = new javax.swing.JScrollPane();
        jtListarLivros = new javax.swing.JTable();

        setTitle("Biblioteca Mendes - Listar Obras ");
        setPreferredSize(new java.awt.Dimension(743, 463));
        setResizable(false);

        jtListarLivros.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jtListarLivros.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        jtListarLivros.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nome", "Lingua", "Midia", "Seção", "Autor"
            }
        ));
        jScrollPane2.setViewportView(jtListarLivros);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 731, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 371, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TelaListaObras.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TelaListaObras.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TelaListaObras.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaListaObras.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new TelaListaObras().setVisible(true);
                } catch (NaoEncontradoException ex) {
                    Logger.getLogger(TelaListaObras.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jtListarLivros;
    // End of variables declaration//GEN-END:variables
}
