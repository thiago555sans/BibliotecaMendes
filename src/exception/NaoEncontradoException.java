package exception;

public class NaoEncontradoException extends Exception {
	
	public NaoEncontradoException () {
		super("Obra nao encontrada");
	}

}
