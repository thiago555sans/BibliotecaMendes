package exception;

public class AutorNaoEncontradoException extends Exception{
	
	public AutorNaoEncontradoException() {
		super("Autor nao encontrado");
	}

}
