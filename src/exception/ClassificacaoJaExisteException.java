package exception;

public class ClassificacaoJaExisteException extends Exception {
	
	public ClassificacaoJaExisteException() {
		super("Classificacao Ja cadastrada");
	}

}
